import numpy as np
from decimal import Decimal
from bokeh.layouts import column, row
from bokeh.models import ColumnDataSource, CustomJS, Div, FuncTickFormatter, RadioButtonGroup, Slider, TextInput
from bokeh.plotting import curdoc, figure, output_file, show
from bokeh.themes import Theme

viscosity = 3e-6  # viscosity of hydrogen plasma at 10^4K
laser_speed = 500
laser_density = 0.1
laser_temperature = 1e6
laser_size = 5e-3
plasma_temp = 10 ** 4


def calc_v(eta, v_jet, v_bs_bool):
    v_bs = v_jet / (1 + (eta ** -0.5))
    return np.abs(v_jet * v_bs_bool - v_bs)


def calc_mach(eta, v_jet, v_bs_bool):
    return calc_v(eta, v_jet, v_bs_bool) / 20


def calc_reyolds(eta, v_jet, n_a, size, v_bs_bool):
    density = n_a * (1 + (v_bs_bool * (eta - 1)))
    return calc_v(eta, v_jet, v_bs_bool) * 1e3 * density * size / viscosity


def calc_c_length(eta, v_jet, n_a, size, temperature, v_bs_bool):
    density = n_a * (1 + (v_bs_bool * (eta - 1)))  # using the  8.35e12 const to avoid precision error
    return 9 * (calc_v(eta, v_jet, v_bs_bool) * 1e3) ** 3 * 0.62 * 1.67e-27 / (
                64 * 2e-40 * density * np.sqrt(temperature))


def calc_n_shell(v):
    return (0.62 * 1.6e-27 * v ** 2) / (1.38e-23 * 8000)


def calc_t_time(eta, v_jet, n_a, size, wavelength, v_bs_bool):
    v = calc_v(eta, v_jet, v_bs_bool) * 1e3
    n_shell = calc_n_shell(v)
    t_time = np.sqrt(1 / (2 * np.pi) * (n_shell / eta) * (1 / v ** 2) * (size * wavelength / (3)))
    return t_time


def calc_t_time_ratio(eta, v_jet, n_a, size, wavelength, v_bs_bool):
    v = calc_v(eta, v_jet, v_bs_bool) * 1e3
    n_shell = calc_n_shell(v)
    t_time = np.sqrt(1 / (2 * np.pi) * (n_shell / eta) * ((v_jet * 1000) ** 2 / v ** 2) * (wavelength / (3 * size)))
    return t_time


def calc_t_cond(eta, v_jet, n_a, size, wavelength, temperature, v_bs_bool):
    density = n_a * (1 + (v_bs_bool * (eta - 1)))
    c_time = 9 * (calc_v(eta, v_jet, v_bs_bool) * 1e3) ** 2 * 0.62 * 1.67e-27 / (16 * density * 2e-40 * 10 ** 2)
    t_cond = 4 * 10 ** 6 * (20 * c_time / size) * (size / wavelength) ** 2 * temperature ** 2 / (size * density)
    return t_cond


TOOLS = "pan,wheel_zoom,box_zoom,reset,save,box_select"

bg1 = RadioButtonGroup(labels=["Density Ratio", "Ambient Density"], active=0)
bg2 = RadioButtonGroup(labels=["Bow shock", "Reverse shock"], active=0)
bg3 = RadioButtonGroup(labels=["Astro scaled", "Lab scaled"], active=0)

opts = {
    0: np.logspace(-2, 2, 1000, base=10),
    1: np.logspace(8, 17, 1000, base=10.0),
}
range = {
    0: (0.01, 100),
    1: (1e8, 1e17)
}

v_jet_slider = Slider(title="Jet speed (km)", start=0, end=3, value=0.5, step=0.1,
                      format=FuncTickFormatter(code="return Math.pow(10,tick).toFixed(2)"))
n_a_slider = Slider(title="Ambient density (m^-3)", start=8, end=20, value=9, step=0.5,
                    format=FuncTickFormatter(code="return Math.pow(10,tick).toFixed(2)"))
eta_slider = Slider(title="Density ratio", start=-2, end=2, value=1, step=0.1,
                    format=FuncTickFormatter(code="return Math.pow(10,tick).toFixed(2)"))
size_slider = Slider(title="Jet Size (m)", start=10, end=15, value=10, step=0.5,
                     format=FuncTickFormatter(code="return Math.pow(10,tick).toFixed(2)"))
wavelength_slider = Slider(title="Wavelength (m)", start=9, end=15, value=10, step=0.5,
                           format=FuncTickFormatter(code="return Math.pow(10,tick).toFixed(2)"))

p_mach = figure(title="Mach Number", tools=TOOLS, y_axis_type="log", x_axis_type="log")
p_mach_line = p_mach.line(x=opts[0], y=calc_mach(opts[0], 10 ** v_jet_slider.value, bg2.active))
p_mach.yaxis.axis_label = "Mach Number"

p_reynolds = figure(title="Reynolds Number", tools=TOOLS, y_axis_type="log", x_axis_type="log")
p_reynolds_line = p_reynolds.line(x=opts[0], y=calc_reyolds(opts[0], 10 ** v_jet_slider.value,
                                                            10 ** n_a_slider.value, 10 ** size_slider.value,
                                                            bg2.active))
p_reynolds.yaxis.axis_label = "Reynolds Number"

p_taylor_time = figure(title="Taylor time", tools=TOOLS, y_axis_type="log", x_axis_type="log")
p_taylor_time_line = p_taylor_time.line(x=opts[0], y=calc_t_time(opts[0], 10 ** v_jet_slider.value,
                                                                 10 ** n_a_slider.value,
                                                                 10 ** size_slider.value,
                                                                 10 ** wavelength_slider.value,
                                                                 bg2.active))
p_taylor_time.yaxis.axis_label = "t_RT"

p_taylor_ratio_time = figure(title="Taylor time ratio", tools=TOOLS, y_axis_type="log", x_axis_type="log")
p_taylor_ratio_time_line = p_taylor_ratio_time.line(x=opts[0],
                                                    y=calc_t_time_ratio(opts[0], 10 ** v_jet_slider.value,
                                                                        10 ** n_a_slider.value,
                                                                        10 ** size_slider.value,
                                                                        10 ** wavelength_slider.value,
                                                                        bg2.active))
p_taylor_ratio_time.yaxis.axis_label = "t_RT/t_j"

p_cooling_time = figure(title="Cooling length", tools=TOOLS, y_axis_type="log", x_axis_type="log")

p_cooling_time_line = p_cooling_time.line(x=opts[0],
                                          y=calc_c_length(opts[0], 10 ** v_jet_slider.value,
                                                          10 ** n_a_slider.value, 10 ** size_slider.value,
                                                          plasma_temp, bg2.active))
p_cooling_time.yaxis.axis_label = "Cooling length (m)"

p_thermal_time = figure(title="Conduction timescale ratio", tools=TOOLS, y_axis_type="log", x_axis_type="log")

p_thermal_time_line = p_thermal_time.line(x=opts[0],
                                          y=calc_t_cond(opts[0], 10 ** v_jet_slider.value,
                                                        10 ** n_a_slider.value, 10 ** size_slider.value,
                                                        10 ** wavelength_slider.value, plasma_temp, bg2.active))
p_thermal_time.yaxis.axis_label = "t_dyn/t_cond"

laser_info = Div(text="""<h4> Laser experiment parameters (taken from PALS)</h4><br/>
<b>Jet speed:</b> 500km/s<br/>
<b>Jet density:</b> 0.1kg/m^3<br/>
<b>Jet temperature:</b> 10 ^6K <br/>
<b>Jet size:</b> 5*10^-3m""",
                 width=200, height=100)

speed_scaler = TextInput(value="0.2", title="Velocity scaling param:")
density_scaler = TextInput(value="1e-19", title="Density scaling param:")
temperature_scaler = TextInput(value="1e-2", title="Temperature scaling param:")
size_scaler = TextInput(value="2e17", title="Temperature scaling param:")


def update(attr, old, new):
    x = opts[bg1.active]
    p_mach_line.data_source.data['x'] = x
    p_reynolds_line.data_source.data['x'] = x
    p_taylor_ratio_time_line.data_source.data['x'] = x
    p_taylor_time_line.data_source.data['x'] = x
    p_cooling_time_line.data_source.data['x'] = x
    p_thermal_time_line.data_source.data['x'] = x
    velocity = 10 ** v_jet_slider.value * (1 + ((1 / float(speed_scaler.value) - 1) * bg3.active))
    density = float(Decimal(10 ** n_a_slider.value) * (1 + ((1 / Decimal(density_scaler.value) - 1) * bg3.active)))
    temperature = plasma_temp * (1 + ((1 / float(temperature_scaler.value) - 1) * bg3.active))
    size = float(Decimal(10 ** size_slider.value) * (1 + ((1 / Decimal(size_scaler.value) - 1) * bg3.active)))
    wavelength = float(
        Decimal(10 ** wavelength_slider.value) * (1 + ((1 / Decimal(size_scaler.value) - 1) * bg3.active)))
    if bg1.active:
        p_mach_line.data_source.data['y'] = np.repeat(
            calc_mach(10 ** eta_slider.value, 10 ** v_jet_slider.value, bg2.active), 1000)

        p_reynolds_line.data_source.data['y'] = calc_reyolds(10 ** eta_slider.value, velocity,
                                                             opts[1], size,
                                                             bg2.active)

        p_taylor_time_line.data_source.data['y'] = np.repeat(
            calc_t_time(10 ** eta_slider.value, velocity,
                        opts[1], size,
                        wavelength, bg2.active), 1000)
        p_taylor_ratio_time_line.data_source.data['y'] = np.repeat(calc_t_time_ratio(10 * eta_slider.value,
                                                                                     velocity,
                                                                                     opts[1],
                                                                                     size,
                                                                                     wavelength,
                                                                                     bg2.active), 1000)
        p_cooling_time_line.data_source.data['y'] = calc_c_length(10 ** eta_slider.value, velocity,
                                                                  opts[1], size, temperature,
                                                                  bg2.active)
        p_thermal_time_line.data_source.data['y'] = calc_t_cond(10 ** eta_slider.value, velocity,
                                                                opts[1], size,
                                                                wavelength, temperature, bg2.active)
    else:
        p_mach_line.data_source.data['y'] = calc_mach(opts[0], 10 ** v_jet_slider.value, bg2.active)
        p_reynolds_line.data_source.data['y'] = calc_reyolds(opts[0], velocity,
                                                             density, size,
                                                             bg2.active)
        p_taylor_time_line.data_source.data['y'] = calc_t_time(opts[0], velocity,
                                                               density, size,
                                                               wavelength, bg2.active)
        p_taylor_ratio_time_line.data_source.data['y'] = calc_t_time_ratio(opts[0], velocity,
                                                                           density,
                                                                           size,
                                                                           wavelength, bg2.active)
        p_cooling_time_line.data_source.data['y'] = calc_c_length(opts[0], velocity,
                                                                  density, size, temperature,
                                                                  bg2.active)
        p_thermal_time_line.data_source.data['y'] = calc_t_cond(opts[0], velocity, density, size,
                                                                wavelength, temperature, bg2.active)


bg1.on_change('active', update)
bg2.on_change('active', update)
bg3.on_change('active', update)
v_jet_slider.on_change('value', update)
n_a_slider.on_change('value', update)
eta_slider.on_change('value', update)
size_slider.on_change('value', update)
wavelength_slider.on_change('value', update)

layout = row(
    column(v_jet_slider, n_a_slider, eta_slider, size_slider, wavelength_slider),
    column(bg1, bg2, bg3, p_mach, p_reynolds, p_taylor_time,
           p_taylor_ratio_time, p_cooling_time, p_thermal_time),
    column(laser_info),
    column(speed_scaler, density_scaler, temperature_scaler, size_scaler))
curdoc().add_root(layout)
