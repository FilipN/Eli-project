# Astrophysical Jet Scaling Visualization Tool

## Dependencies
Requires
- Numpy
- Bokeh [link](https://docs.bokeh.org/en/latest/docs/installation.html)
- Decimal
### User guide
Runnable by command (bokeh has to be installed)
```
bokeh serve --show jet_plot.py
```

